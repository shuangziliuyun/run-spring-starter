package com.william.runspringstarter.controller;

import com.william.springstartdemo.service.ExampleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jianweilin
 * @date 2018/11/25
 */
@RestController
public class IndexController {

    @Autowired
    private ExampleService exampleService;

    @RequestMapping(value="/index",method= RequestMethod.GET)
    public String index(String content){
        return exampleService.wrap(content);
    }

}
