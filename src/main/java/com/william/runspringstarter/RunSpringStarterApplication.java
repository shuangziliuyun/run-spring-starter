package com.william.runspringstarter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RunSpringStarterApplication {

	public static void main(String[] args) {
		SpringApplication.run(RunSpringStarterApplication.class, args);
	}
}
